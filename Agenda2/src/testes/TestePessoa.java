package testes;

import static org.junit.Assert.*;

import Model.Pessoa;

import org.junit.Before;
import org.junit.Test;


public class TestePessoa {
	
private Pessoa umaPessoa;
	
	@Before
	public void setUp()throws Exception{
		umaPessoa = new Pessoa();	
	}
	
	@Test
	public void testNome(){
		umaPessoa.setNome("Fulano");
		assertEquals("Fulano", umaPessoa.getNome());
	}
	
	@Test
	public void testTelefone(){
		umaPessoa.setTelefone("0000-0000");
		assertEquals("0000-0000", umaPessoa.getTelefone());
	}
	
	@Test
	public void testIdade(){
		umaPessoa.setIdade("18");
		assertEquals("18",umaPessoa.getIdade());
	}
	
	@Test
	public void testSexo(){
		umaPessoa.setSexo("F");
		assertEquals("F", umaPessoa.getSexo());
	}
	
	@Test
	public void testEmail(){
		umaPessoa.setEmail("fulano@gmail.com");
		assertEquals("fulano@gmail.com", umaPessoa.getEmail());
	}
	
	@Test
	public void testHangout(){
		umaPessoa.setHangout("000-000");
		assertEquals("000-000", umaPessoa.getHangout());
	}
	@Test
	public void testEndereco(){
		umaPessoa.setEndereco("gama");
		assertEquals("gama", umaPessoa.getEndereco());
	}
	
	@Test
	public void testRg(){
		umaPessoa.setRg("2934712");
		assertEquals("2934712", umaPessoa.getRg());
	}
	
	@Test
	public void testCpf(){
		umaPessoa.setCpf("011571290");
		assertEquals("011571290", umaPessoa.getCpf());
	}
}
