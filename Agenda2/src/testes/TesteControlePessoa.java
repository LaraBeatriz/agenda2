package testes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Model.ControlePessoa;
import Model.Pessoa;


public class TesteControlePessoa {
	
	private ControlePessoa umControlePessoa;
	private Pessoa umaPessoa;
		
		@Before
		public void setUp()throws Exception{
			umControlePessoa = new ControlePessoa();
			umaPessoa = new Pessoa();
		}
		
		@Test
		public void testeAdicionar(){
			assertEquals("Pessoa adicionada com Sucesso!",umControlePessoa.adicionar(umaPessoa));
		}
		
		@Test
		public void testeRemover() {		
			assertEquals("Pessoa removida com Sucesso!", umControlePessoa.remover(umaPessoa));	
		}
		
		@Test
		public void testPesquisar() {
			String umNome = "Fulano";
			umaPessoa.setNome(umNome);
			umControlePessoa.adicionar(umaPessoa);
			
			assertEquals(umaPessoa, umControlePessoa.pesquisar(umNome));
			
		}	

}
