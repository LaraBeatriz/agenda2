package Model;

import java.util.ArrayList;

public class ControlePessoa {

//atributos

    private ArrayList<Pessoa> listaPessoas;

//construtor

    public ControlePessoa() {
        listaPessoas = new ArrayList<Pessoa>();
    }

// métodos
   
    public void adicionar(Pessoa umaPessoa) {
        System.out.println("Pessoa adicionada com Sucesso!");
        listaPessoas.add(umaPessoa);

    }

    public void remover(Pessoa umPessoa) {
        listaPessoas.remove(umPessoa);
    }
    
    public Pessoa pesquisar(String nome) {
        for (Pessoa pessoa: listaPessoas) {
            if (pessoa.getNome().equalsIgnoreCase(nome)) return pessoa;
        }
        System.out.println("Pessoa não encontrada");
        return null;
    }

}
