
public class ConversorTemperatura {

	public double celsiusParaFahrenheit (double valorCelsius){
		return 9*(valorCelsius/5)+32;
	}
	
	public double fahrenheitParaCelsius (double valorFahrenheit){
		return 5*(valorFahrenheit-32)/9;	
	}
	
	public double celsiusParaKelvin (double valorCelsius){
		return valorCelsius+273;
	}
	
	public double kelvinParaCelsius (double valorKelvin){
		return valorKelvin-273;
	}
	
	public double kelvinParaFahrenheit(double valorKelvin){
		return 9*(valorKelvin-273)/5+32;
	}
	
	public double FahrenheitParaKelvin (double valorFahrenheit){
		return 5*(valorFahrenheit-32)/9+273;
	}
}
